<?php

/**
 * Implements hook_menu().
 */
function words_menu() {

  $items = array();

  $items['admin/config/development/words'] = array(
    'title' => t('Words settings'),
    'description' => t('Manage list of words'),
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('words_settings_form'),
    'access arguments' => array('administer site configuration'),
  );

  return $items;

} // words_menu

/**
 * Provides settings form.
 */
function words_settings_form($form, &$form_state) {

  $form = array();

  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('Word list file'),
    '#description' => t('.txt file containing one word per line')
  );

  $form['clear'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear current words'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;

} // words_settings_form

/**
 * Handles settings form submission.
 */
function words_settings_form_submit($form, &$form_state) {

  $words = 0;

  if ($form_state['values']['clear'] == 1) {

    db_truncate('words')->execute();
    drupal_set_message('Cleared old words.');

  } // if

  $file = file_save_upload('file');
  $path = drupal_realpath($file->uri);
  $lines = file($path);

  foreach ($lines as $line) {

    $word = trim($line);
    $length = strlen($word);

    if ($length > 0) {

      try {

        db_merge('words')
          ->key(array('word' => $word))
          ->fields(array(
            'length' => $length,
          ))
          ->execute();

          $words++;

      } catch (Exception $e) {
        
        // PDO Exceptions are thrown for non-ASCII characters. Not sure what to do about that.

      }

    } // if

  } // foreach

  drupal_set_message(format_plural($words, 'Added 1 word.', 'Added @count words.'));

} // words_settings_form_submit

/**
 * Searches for words matching conditions.
 */
function words_search($conditions, $execute = TRUE) {

  $query = db_select('words', 'w')->fields('w');

  foreach ($conditions as $type => $value) {

    switch ($type) {
      case 'length': 
        $query->condition('w.length', $value); 
        break;
      case 'minimum':
        $query->condition('w.length', $value, '>=');
        break;
      case 'maximum':
        $query->condition('w.length', $value, '<=');
        break;
      case 'word': 
        $query->condition('w.word', $value);
        break;
      case 'start': 
        $query->condition('w.word', $value . '%', 'LIKE');
        break;
      case 'end':
        $query->condition('w.word', '%' . $value, 'LIKE');
        break;
      case 'contains':
        $query->condition('w.word', '%' . $value . '%', 'LIKE');
        break;
    } // switch

  } // foreach

  if ($execute) {

    $words = array();
    $results = $query->execute();

    foreach ($results as $result) {
      $words[] = $result;
    }

    return $words;

  } // if

  return $query;

} // words_search
